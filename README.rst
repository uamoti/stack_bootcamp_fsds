======================================================
Bootcamp fullstack data scientist by Stack Tecnologias
======================================================

O bootcamp oferecido pela Stack Tecnologias teve como intuito introduzir a realidade
de um profissional fullstack na área de ciência de dados.

O problema apresentado foi na área de gestão de RH: como reduzir a evasão de funcionários de uma empresa?

As tecnologias usadas para desenvolver a solução foram:

- Python (pandas, scikit-learn, matplotlib, seaborn, pycaret)

- Apache Airflow

- MinIO

- Streamlit

- Docker

A idéia geral foi agregar dados de diferentes fontes como  MySQL, json e xlsx, analisar os parâmetros relevantes
para o problema e explorar algoritmos de ML para realizar predições.

Docker foi usado para rodar os armazenamentos na forma de MySQL e MinIO, e agendar as tarefas com o Apache Airflow.

