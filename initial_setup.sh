## Install and configure MySQL server
# Create MySQL container and enable port 3307
docker run --name stackdb -e MYSQL_ROOT_PASSWORD=bootcamp -p "3307:3306" -d mysql

## Install and configure datalake with MinIO server
# username: minioadmin
# password: minioadmin
docker run --name minio -d -p 9000:9000 -p 9001:9001 -v "$PWD/datalake:/data" minio/minio server /data --console-address ":9001"

## Install and configure Apache Airflow
docker run -d -p 8080:8080 -v "$PWD/airflow/dags:/opt/airflow/dags/" --entry-point=/bin/bash --name airflow apache/airflow:2.1.1-python3.8 \
    -c "(airflow db init && airflow users create --username admin --password stack --firstname Bernardo --lastname Oliveira --role Admin " \
    "--email admin@example.org); airflow webserver & airflow scheduler"

# Connect to airflow container
docker container exec -it airflow bash

# Install Python libraries
pip install pymysql xlrd openpyxl minio


